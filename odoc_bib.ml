(*********************************************************************************)
(*                OCamldoc-generators                                            *)
(*                                                                               *)
(*    Copyright (C) 2012 Institut National de Recherche en Informatique          *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License version 3             *)
(*    or later as published by the Free Software Foundation.                     *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software Foundation,    *)
(*    Inc., 59 Temple Place, Suite 330, Boston, MA                               *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** Custom ocamldoc generator, handling .bib files. to reference to .bib entries
   in ocamldoc comments.

   - .bib files are given with a new -bib option on command line.
   - a .bib entry is referenced with \{bib ...}
*)

let bib_entries = ref [];;
let bib_index_file = "bibliography.html";;

(*c==v=[String.no_blanks]=1.0====*)
let no_blanks s =
  let len = String.length s in
  let buf = Buffer.create len in
  for i = 0 to len - 1 do
    match s.[i] with
      ' ' | '\n' | '\t' | '\r' -> ()
    | c -> Buffer.add_char buf c
  done;
  Buffer.contents buf
(*/c==v=[String.no_blanks]=1.0====*)

exception Unknown_bib_ref of string

let get_entry id =
  try List.find (fun t -> t.Bibtex.id = id) !bib_entries
  with Not_found -> raise (Unknown_bib_ref id)
;;


let bp = Printf.bprintf;;

let print_bib_ref buf t =
  match t with
    [Odoc_info.Raw id] ->
      begin
        let id = no_blanks id in
        try
          let entry = get_entry id in
          Printf.bprintf buf
            "<a href=\"%s#%s\"><i>[%s]</i></a>"
            bib_index_file entry.Bibtex.id entry.Bibtex.id
        with
          Unknown_bib_ref id ->
            Odoc_info.warning
              (Printf.sprintf "Unknown bibtex id %s" id);
            Printf.bprintf buf "<i>[?%s?]</i>" id
      end
  | _ ->
      ()
;;

let get_field name fields =
  let name = String.lowercase name in
  try List.assoc name fields
  with Not_found -> ""
;;

module Generator (G : Odoc_html.Html_generator) =
struct
class html =
  object(self)
    inherit G.html as super

    method html_of_custom_text b s t =
      match s with
      | "{bib" | "bib" -> print_bib_ref b t
      | _ -> ()

    method private print_bib_entry buf e =
      bp buf "<div class=\"bibentry\">\n";
      bp buf "<div class=\"bibid\"><span id=\"%s\">[%s]</span></div>\n"
        (self#escape e.Bibtex.id)
        (self#escape e.Bibtex.id);
      begin
      match self#escape (get_field "title" e.Bibtex.fields) with
          "" -> ();
        | title ->
            let title =
              match get_field "url" e.Bibtex.fields with
                "" -> title
              | url -> Printf.sprintf "<a href=\"%s\">%s</a>" url title
            in
            bp buf "<div class=\"bibtitle\">%s</div>\n" title
      end;
      bp buf "<div class=\"bibkind\">[%s]</div>\n"
        (self#escape (String.lowercase e.Bibtex.kind));
      bp buf "<div class=\"bibauthor\">%s</div>\n"
        (self#escape (get_field "author" e.Bibtex.fields));
      bp buf "</div>\n"

    method private generate_html_bib file entries =
      try
        let chanout = open_out file in
        let b = Buffer.create 1024 in
        Buffer.add_string b "<html>";
        self#print_header b (self#inner_title "Bibliography");
        Buffer.add_string b "<body>\n";
        Buffer.add_string b "<h1>Bibliography</h1>\n";
        let entries = List.sort
          (fun e1 e2 ->
             compare
             (String.lowercase (get_field "title" e1.Bibtex.fields))
             (String.lowercase (get_field "title" e2.Bibtex.fields))
          )
          entries
        in
        List.iter (self#print_bib_entry b) entries;
        Buffer.add_string b "</body></html>";
        Buffer.output_buffer chanout b;
        close_out chanout
      with
        Sys_error s ->
          incr Odoc_info.errors ;
          prerr_endline s

(*
    val mutable dot_cpt = 0
    method html_of_Module_list b l =
        Buffer.add_string b "<table><tr><td>";
        super#html_of_Module_list b l;
        Buffer.add_string b "</td><td>";
        dot_cpt <- dot_cpt + 1;
        let dot_file = Filename.concat !Odoc_info.Global.target_dir
          (Printf.sprintf "dot%d.dot" dot_cpt)
        in
        let png_file = Printf.sprintf "%s.png" (Filename.chop_extension dot_file) in
        let png_file_tmp = Printf.sprintf "%s.tmp" (Filename.chop_extension dot_file) in
        Odoc_info.Global.out_file := dot_file ;
        let dot_gen = new Odoc_dot.dot in
        let modules =
        List.fold_left
            (fun acc name ->
               try
                 (List.find (fun m -> m.Odoc_module.m_name = name) self#list_modules) :: acc
               with
                 Not_found -> acc
            ) [] l
        in
        dot_gen#generate modules;
        let com = Printf.sprintf "dot -Grotate=0 -Tpng -o %s %s && convert -scale 650x500 %s %s"
          (Filename.quote png_file_tmp) (Filename.quote dot_file)
          (Filename.quote png_file_tmp) (Filename.quote png_file)
        in
        ignore(Sys.command com);
        Printf.bprintf b "<img src=\"%s\"></img>" (Filename.basename png_file);
        Buffer.add_string b "</td></tr></table>\n"
*)
    method generate mods =
      super#generate mods;
      self#generate_html_bib
        (Filename.concat !Odoc_info.Global.target_dir bib_index_file)
       !bib_entries

    initializer
      let html_of_control text =
        let b = Buffer.create 256 in
        self#html_of_text b text;
        Printf.sprintf "<b>Control: </b>%s<br/>" (Buffer.contents b)
      in
      tag_functions <- ("control", html_of_control) :: tag_functions;

      let html_of_todo text =
        let b = Buffer.create 256 in
        self#html_of_text b text;
        Printf.sprintf "<b>TODO: </b>%s<br/>" (Buffer.contents b)
      in
      tag_functions <- ("todo", html_of_todo) :: tag_functions;
  end
end;;

let add_bib_file file =
  try
    Odoc_info.verbose (Printf.sprintf "loading bibtex file %s" file);
    let inch = open_in file in
    let lexbuf = Lexing.from_channel inch in
    let entries =
      try Bib_parser.entries Bib_lexer.main lexbuf
      with
        | Bib_parser.Error ->
          let pos = lexbuf.Lexing.lex_curr_p in
          let msg = Printf.sprintf "Parse error line %d, character %d"
            pos.Lexing.pos_lnum (pos.Lexing.pos_cnum - pos.Lexing.pos_bol)
          in
          failwith msg
    in
    close_in inch;
    bib_entries := !bib_entries @ entries
  with
    Sys_error s
  | Failure s ->
      prerr_endline s ;
      exit 1
;;

Odoc_args.add_option
  ("-bib", Arg.String add_bib_file, "<file> add entries from the givne .bib file");;

let _ = Odoc_args.extend_html_generator (module Generator : Odoc_gen.Html_functor)
