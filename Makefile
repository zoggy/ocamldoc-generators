#################################################################################
#                OCamldoc-generators                                            #
#                                                                               #
#    Copyright (C) 2012 Institut National de Recherche en Informatique          #
#    et en Automatique. All rights reserved.                                    #
#                                                                               #
#    This program is free software; you can redistribute it and/or modify       #
#    it under the terms of the GNU General Public License version 3             #
#    or later as published by the Free Software Foundation.                     #
#                                                                               #
#    This program is distributed in the hope that it will be useful,            #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#    GNU General Public License for more details.                               #
#                                                                               #
#    You should have received a copy of the GNU General Public License          #
#    along with this program; if not, write to the Free Software Foundation,    #
#    Inc., 59 Temple Place, Suite 330, Boston, MA                               #
#    02111-1307  USA                                                            #
#                                                                               #
#    Contact: Maxence.Guesdon@inria.fr                                          #
#                                                                               #
#                                                                               #
#################################################################################

MKDIR=mkdir -p
CP=cp -f

OCAMLFIND=ocamlfind
OCAMLC=ocamlc -annot
OCAMLOPT=ocamlopt -annot
OCAMLDOC=ocamldoc
OCAMLLEX=ocamllex

MENHIR=menhir --table
MENHIR_INCLUDES=`$(MENHIR) --suggest-comp-flags`
MENHIR_LINK_FLAGS=`$(MENHIR) --suggest-link-flags-opt`
MENHIR_LINK_FLAGS_BYTE=`$(MENHIR) --suggest-link-flags-byte`

INCLUDES=$(MENHIR_INCLUDES) -I +ocamldoc
COMPFLAGS=$(INCLUDES)
LINKFLAGS=$(INCLUDES) $(MENHIR_LINK_FLAGS)
LINKFLAGS_BYTE=$(INCLUDES) $(MENHIR_LINK_FLAGS_BYTE)

PACKAGES=
OF_FLAGS=

PLUGINS=odoc_literate.cmxs odoc_todo.cmxs odoc_bib.cmxs odoc_snippet.cmxs
PLUGINS_BYTE=odoc_literate.cmo odoc_todo.cmo odoc_bib.cma odoc_snippet.cmo

all: byte opt
opt: $(PLUGINS)
byte: $(PLUGINS_BYTE)

odoc_bib.cmx: bibtex.cmi bib_parser.cmi bib_parser.cmx bib_lexer.cmx odoc_bib.ml
	$(OCAMLFIND) ocamlopt -c $(LINKFLAGS) odoc_bib.ml

odoc_bib.cmxs: bibtex.cmi bib_parser.cmi bib_parser.cmx bib_lexer.cmx odoc_bib.cmx
	$(OCAMLFIND) ocamlopt -shared -o $@  $(LINKFLAGS) `ls $^ | grep -v cmi`

odoc_bib.cma: bibtex.cmi bib_parser.cmi bib_parser.cmo bib_lexer.cmo odoc_bib.cmo
	$(OCAMLFIND) ocamlc -a -o $@  $(LINKFLAGS_BYTE) `ls $^ | grep -v cmi`

odoc_literate.cmxs: odoc_literate.ml
	$(OCAMLFIND) ocamlopt -shared -o $@ $(INCLUDES) $<

odoc_todo.cmxs: odoc_todo.ml
	$(OCAMLFIND) ocamlopt -shared -o $@ $(INCLUDES) $<

odoc_snippet.cmxs: odoc_snippet.ml
	$(OCAMLFIND) ocamlopt -shared -o $@ $(INCLUDES) $<

install:
	$(MKDIR) `$(OCAMLDOC) -customdir`
	$(CP) $(PLUGINS_BYTE) $(PLUGINS) `$(OCAMLDOC) -customdir`

distclean: clean

clean:
	rm -f *.cm* *.o *.annot bib_parser.ml bib_parser.mli bib_lexer.ml

bib_parser.ml bib_parser.mli: bib_parser.mly
	$(MENHIR) $<

# headers :
###########
HEADFILES= Makefile *.ml*
headers: dummy
	echo $(HEADFILES)
	headache -h header.txt -c .headache_config `ls $(HEADFILES) `

noheaders: dummy
	headache -r -c .headache_config `ls $(HEADFILES) `

dummy:

# Rules
.SUFFIXES: .mli .ml .cmi .cmo .cmx .mll .mly

%.cmi:%.mli
	$(OCAMLFIND) ocamlc $(OF_FLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmo:%.ml
	$(OCAMLFIND) ocamlc $(OF_FLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmi %.cmo:%.ml
	$(OCAMLFIND) ocamlc $(OF_FLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmx %.o:%.ml
	$(OCAMLFIND) ocamlopt $(OF_FLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.ml:%.mll
	$(OCAMLLEX) $<

%.mli %.ml:%.mly
	$(OCAMLYACC) -v $<

.PHONY: clean depend

.depend depend:
	ocamldep *.ml > .depend


include .depend
