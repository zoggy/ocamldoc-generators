(*********************************************************************************)
(*                OCamldoc-generators                                            *)
(*                                                                               *)
(*    Copyright (C) 2012 Institut National de Recherche en Informatique          *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License version 3             *)
(*    or later as published by the Free Software Foundation.                     *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software Foundation,    *)
(*    Inc., 59 Temple Place, Suite 330, Boston, MA                               *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** An OCamldoc generator to add random snippets in generated pages.*)

open Odoc_info
module Naming = Odoc_html.Naming
open Odoc_info.Value
open Odoc_info.Module

let p = Printf.bprintf

(*c==v=[String.split_string]=1.1====*)
let split_string ?(keep_empty=false) s chars =
  let len = String.length s in
  let rec iter acc pos =
    if pos >= len then
      match acc with
        "" -> []
      | _ -> [acc]
    else
      if List.mem s.[pos] chars then
        match acc with
          "" ->
            if keep_empty then
              "" :: iter "" (pos + 1)
            else
              iter "" (pos + 1)
        | _ -> acc :: (iter "" (pos + 1))
      else
        iter (Printf.sprintf "%s%c" acc s.[pos]) (pos + 1)
  in
  iter "" 0
(*/c==v=[String.split_string]=1.1====*)


let min_lines = ref 2 ;;
let max_lines = ref 18 ;;
let snippet_height = ref "250px";;
let snippet_width = ref "50%";;

let generate_js_code filename values =
  let oc = open_out (Filename.concat !Odoc_info.Global.target_dir filename) in
  output_string oc
"function Snippet (name, url) {
  this.name = name ;
  this.url = url ;
}

list_snippets = new Array();
function add_snippet(name, url) {
  var o = new Snippet(name, url);
  list_snippets.push(o);
}
function display_random_snippet () {
  var randomIndex = Math.floor(Math.random() * list_snippets.length);
  var snippet = list_snippets[randomIndex];
  var iframe = document.getElementById('odoc_random_snippet_code');
  iframe.src = snippet.url;
  iframe.src = iframe.src ;
  document.getElementById('odoc_random_snippet_name').innerHTML = snippet.name;
}
";
  let f_add value =
    Printf.fprintf oc "add_snippet (%s, %s);\n"
    (Filename.quote value.val_name)
    (Filename.quote (Naming.file_code_value_complete_target value))
  in
  List.iter f_add values;
  close_out oc
;;

let js_code_filename = "odoc_snippet.js";;

module Generator (G : Odoc_html.Html_generator) =
struct
class html =
  object (self)
    inherit G.html as html

    (* It would be cool to have a better way to insert custom stuff in the body... *)
    method generate_for_module pre post modu =
      try
        Odoc_info.verbose ("Generate for module "^modu.m_name);
        let (html_file, _) = Naming.html_files modu.m_name in
        let type_file = Naming.file_type_module_complete_target modu.m_name in
        let code_file = Naming.file_code_module_complete_target modu.m_name in
        let chanout = open_out (Filename.concat !Odoc_info.Global.target_dir html_file) in
        let b = Odoc_html.new_buf () in
        let pre_name = Odoc_html.opt (fun m -> m.m_name) pre in
        let post_name = Odoc_html.opt (fun m -> m.m_name) post in
        Odoc_html.bs b doctype ;
        Odoc_html.bs b "<html>\n";
        self#print_header b
          ~nav: (Some (pre_name, post_name, modu.m_name))
          ~comments: (Module.module_comments modu)
          (self#inner_title modu.m_name);
        Odoc_html.bs b "<body>\n" ;
        self#print_navbar b pre_name post_name modu.m_name ;
        Odoc_html.bs b "<center><h1>";
        if modu.m_text_only then
          Odoc_html.bs b modu.m_name
        else
          (
           Odoc_html.bs b
             (
              if Module.module_is_functor modu then
                Odoc_messages.functo
              else
                Odoc_messages.modul
             );
           Odoc_html.bp b " <a href=\"%s\">%s</a>" type_file modu.m_name;
           (
            match modu.m_code with
              None -> ()
            | Some _ -> Odoc_html.bp b " (<a href=\"%s\">.ml</a>)" code_file
           )
          );
        Odoc_html.bs b "</h1></center>\n<br>\n";

        if not modu.m_text_only then self#html_of_module b ~with_link: false modu;

        (* parameters for functors *)
        self#html_of_module_parameter_list b
          (Name.father modu.m_name)
          (Module.module_parameters modu);

        (* a horizontal line *)
        if not modu.m_text_only then Odoc_html.bs b "<hr width=\"100%\">\n";

        (* module elements *)
        p b "<div class=\"module_elements\">\n";
        p b "<div id=\"odoc_snippet_frame\">";
        p b "<div class=\"snippet_name\">A taste of code: <span id=\"odoc_random_snippet_name\"></div>";
        p b "<iframe id=\"odoc_random_snippet_code\" src=\"\"></iframe>\n</div>";
        p b "<script>window.onload=display_random_snippet()</script>\n";
        List.iter
        (self#html_of_module_element b (Name.father modu.m_name))
        (Module.module_elements modu);
        p b "</div>\n";

        Odoc_html.bs b "</body></html>";
        Buffer.output_buffer chanout b;
        close_out chanout;

        (* generate html files for submodules *)
        self#generate_elements  self#generate_for_module (Module.module_modules modu);
        (* generate html files for module types *)
        self#generate_elements  self#generate_for_module_type (Module.module_module_types modu);
        (* generate html files for classes *)
        self#generate_elements  self#generate_for_class (Module.module_classes modu);
        (* generate html files for class types *)
        self#generate_elements  self#generate_for_class_type (Module.module_class_types modu);

        (* generate the file with the complete module type *)
        self#output_module_type
          modu.m_name
          (Filename.concat !Odoc_info.Global.target_dir type_file)
          modu.m_type;

        match modu.m_code with
          None -> ()
        | Some code ->
            self#output_code
              modu.m_name
              (Filename.concat !Odoc_info.Global.target_dir code_file)
              code
      with
        Sys_error s ->
          raise (Failure s)

    (* It would be cool to have a better way to insert custom stuff in the header... *)
    method prepare_header modules =
      html#prepare_header modules;
      let f = header in
      let g b ?nav ?comments t =
        let b2 = Buffer.create 256 in
        f b2 ?nav ?comments t ;
        let s = Buffer.contents b2 in
        let p = String.rindex s '>' in
        let p = p - (String.length "</head>") in
        let s = String.sub s 0 p in
        Printf.bprintf b
        "%s\n<script type=\"text/javascript\" src=\"%s\"></script>\n</head>\n"
        s js_code_filename;
      in
      header <- g

    method generate modules =
      html#generate modules;
      let snippet_values = List.filter
        (fun v ->
           match v.val_code with
             None -> false
           | Some code ->
               let nb_lines= List.length (split_string code ['\n']) in
               !min_lines <= nb_lines && nb_lines <= !max_lines
        )
        html#list_values
      in
      generate_js_code js_code_filename snippet_values

    initializer
      let style =
        [ "div#odoc_snippet_frame { float: right; width: "^ !snippet_width ^";
                                    border-width: 1px 3px 3px 1px ;
                                    border-color: #cccccc #444444 #444444 #cccccc ;
                                    border-style: solid;
                                    margin-left: 5px; margin-right: 5px;
                                  }" ;
          "span#odoc_random_snippet_name { font-weight: bold ; }" ;
          "iframe#odoc_random_snippet_code { border-style: none; width: 100%;
                      margin-right: 10px; height: "^ !snippet_height^"; }" ;
        ]
      in
      default_style_options <- style @ default_style_options

  end
end;;

Odoc_args.add_option
  ("-max-lines", Arg.Set_int max_lines, "<n> set max lines of snippets to keep");;
Odoc_args.add_option
  ("-min-lines", Arg.Set_int min_lines, "<n> set min lines of snippets to keep");;
Odoc_args.add_option
  ("-snippet-width", Arg.Set_string snippet_width,
   "<s> set value for width of snippet div in css file (default is "^ !snippet_width ^")");;
Odoc_args.add_option
  ("-snippet-height", Arg.Set_string snippet_height,
   "<s> set value for height of snippet iframe in css file (default is "^ !snippet_height ^")");;


let _ = Odoc_args.extend_html_generator (module Generator : Odoc_gen.Html_functor)
